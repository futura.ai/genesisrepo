How to: BackTesting and Evaluation
=========================

Backtesting
-----------

We assume that you have already installed the latest version before you start the backtest.

Start testing your system by running python command:

``python path/to/trading_system.py``

or try a couple of examples in our git repo
``python <path to example>/trading_system.py``

The only requirement is to have the following at the end of your python file to make the trading system file self-executable.

.. code-block:: python

	# Evaluate trading system defined in current file.
	if __name__ == '__main__':
		import futuraAiToolbox
	   	results = futuraAiToolbox.evalTradingStrat(__file__)


Alternatively, you can always run the trading system straight from Python console:

``> import futuraAiToolbox``

``> resultMap = futuraAiToolbox.evalTradingStrat('/Path/to/your/TradingSystem.py')``

.. _tradingresults-label:

Trading Results
---------------

``evalTradingStrat`` loads the market data and calls your trading system for each day of the backtest with the most recent market data in our system. It builds the equity curve for your output values ``p``. Finally we returns a detailed map(dictionary) of performance numbers for you to consume. We build the plot by default if you do not want this then you can pass **plotEquity** paramter to ``evalTradingStrat``.

``resultMap`` contains the following fields:

+------------------------------+-----------+--------------------------------------------------+
| Field                        | Data Type | Description                                      |
+==============================+===========+==================================================+
| resultMap['tsName']         | string    | a string that contains the name of your trading  |
|                              |           | system                                           |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['fundDate']       | list      | a list of the dates available to your trading    |
|                              |           | system                                           |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['fundTradeDates'] | list      | a list of the,dates traded by your trading       |
|                              |           | system                                           |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['fundEquity']     | list      | a list defining the equity of your portfolio     |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['marketEquity']   | list      | a list defining the equity earned in each market |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['marketExposure'] | list      | a list containing the equity made from each      |
|                              |           | market in settings['markets']                    |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['errorLog']       | list      | a list describing any errors made in evaluation  |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['runtime']        | float     | a float containing the time required to evaluate |
|                              |           | your system                                      |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['evalDate']       | int       | an int that describes the last date of           |
|                              |           | evaluation for your system (in YYYYMMDD format)  |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['stats']          | dict      | a dict containing the performance statistics of  |
|                              |           | your system                                      |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['settings']       | dict      | a dict containting the settings defined in       |
|                              |           | stratSettings                                       |
+------------------------------+-----------+--------------------------------------------------+
| resultMap['returns']        | list      | a list defining the market returns of your       |
|                              |           | trading system                                   |
+------------------------------+-----------+--------------------------------------------------+
