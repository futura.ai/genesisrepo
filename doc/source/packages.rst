Packages supported by the system
================================

Any numerical packages should be accepted, as long as the quant strategy returns numpy arrays for the exposure and the other settings.

We officially support following packages:

* bokeh
* numpy
* Pandas
* scipy
* scikit-learn
* ta-lib
* keras
* tensorflow
