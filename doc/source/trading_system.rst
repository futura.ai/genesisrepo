Trading System Structure
========================

The trader/strategist defines two functions: ``stratSettings`` and ``tradingStrategy``, and then call the ``evalTradingStrat`` function exposed in the toolbox. The toolbox provides glue code to take in settings and the strategy from your code and run against the data selected in ``market`` key in ``settings``. The evalTradingStrat relies on these two functions to run your trading system. Plese note that the toolbox is sensitive to these two names and failure to provide *exact* name would result in exception raised and failed backtest. The toolbox allows for flexible data definitions, where you can request OPEN, HIGH, LOW, CLOSE, or VOL data for your system. A typical function definition is shown below:

.. py:function:: def tradingStrategy(DATE, HIGH, CLOSE, VOL, exposure, equity, settings)

This ``tradingStrategy`` function will be called each day of the backtesting period with the most recent data as arguments. The system expects ``tradingStrategy`` to return an array of your trading system’s market positions for the next day and the ``settings`` dictionary. Market positions, ``p``, is just an array of numbers [0 1 -1 …] that represents the trading positions you want to take where 0 => no position, 1 => buy position, -1 => sell position.

Arguments/Parameters
--------------------
Data can be requested for your trading system through the arguments of the function definition. The ``tradingStrategy`` function isn’t required to call any arguments, the example above is just a representation of various values that can be called.

Here is a breakdown of what parameters can be loaded into the trading system:

+--------------+--------------------------------------------------+------------------+
| Parameter    | Description                                      | Dimensions       |
|              |                                                  | (rows x columns) |
+==============+==================================================+==================+
| **DATE**     | a date integer in the                            | Lookback x 1     |
|              | format YYYYMMDD                                  |                  |
+--------------+--------------------------------------------------+------------------+
| **OPEN**     | the first price of the session                   | Lookback x       |
|              |                                                  | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **HIGH**     | the highest price of the session                 | Lookback x       |
|              |                                                  | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **LOW**      | the lowest price of the session                  | Lookback x       |
|              |                                                  | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **CLOSE**    | the last price of the session                    | Lookback x       |
|              |                                                  | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **VOL**      | number of stocks/contracts                       | Lookback x       |
|              | traded per session                               | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **exposure** | the realized quantities of your trading system,  | Lookback x       |
|              | or all the trading positions you take            | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **equity**   | cumulative trading performance in each market,   | Lookback x       |
|              | reflects gains and losses                        | # of Markets     |
+--------------+--------------------------------------------------+------------------+
| **OI, R,     | also available to be called as parameters,       |                  |
| RINFO**      | described in more detail under                   |                  |
|              | :ref:`marketdata-label` section                  |                  |
+--------------+--------------------------------------------------+------------------+

The data is structured with the most recent information in the last index or row. Where **lookback** is simply how many historical data points you want to load in each iteration of your trading system. Given the data is daily, lookback represents the maximum number of days of market data you want to have loaded. Now the two parameters that aren’t explicitly related to market data are **exposure** and **equity**. These two relate to trading in your simulated broker account. Whenever you make a trading position, that’s recorded in **exposure**. And whatever the market result of your trading position is, is then recorded in **equity**. Or simply put: **equity** is what you made with each market over your **lookback** period.
