Steps to install the tool box
=============================

For users, simply do:

.. code-block:: python

	pip install futuraAiToolbox


For developers

Developers might want to take a git copy for the bleeding edge code.

* clone the repo at https://gitlab.com/futura.ai/genesisrepo
* `python setup.py install`
* test the example code at examples folder

