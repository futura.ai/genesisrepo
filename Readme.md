
# Table of Contents

1.  [Futuraai python api for quant trading.](#org0e089eb)
2.  [Installation guide](#orgbcb2371)
    1.  [For users:](#orgbcd6285)
    2.  [For developers](#orgeda4c6c)
        1.  [Virtual environment](#orgdf3b2d2)
        2.  [Install the requirements from the 'requirements.txt' file](#org961bf5a)
        3.  [Clone the latest version from gitlab:](#org02f3acb)
        4.  [Make changes!!](#org4e2d164)
    3.  [Install the code without pip:](#org1766d3d)
3.  [Running the Examples:](#org452e1ca)
4.  [API:](#org8032172)


<a id="org0e089eb"></a>

# Futuraai python api for quant trading.

For more information go to our website

www.futura.ai


<a id="orgbcb2371"></a>

# Installation guide


<a id="orgbcd6285"></a>

## For users:

Simply do

```python
pip install futuraAiToolbox
```


<a id="orgeda4c6c"></a>

## For developers

****It is highly recommended that user has a virtual environment setup****


<a id="orgdf3b2d2"></a>

### Virtual environment

For more info on virtualenvs it's  recommended to read
<http://docs.python-guide.org/en/latest/dev/virtualenvs/>


<a id="org961bf5a"></a>

### Install the requirements from the 'requirements.txt' file


<a id="org02f3acb"></a>

### Clone the latest version from gitlab:

```shell
git clone <https://gitlab.com/futura.ai/genesisrepo.git>

```


<a id="org4e2d164"></a>

### Make changes!!


<a id="org1766d3d"></a>

## Install the code without pip:

After you clone the git repository
On the main directory do 
```python
 python setup.py install
```


<a id="org452e1ca"></a>

# Running the Examples:

For running the example
python examples\trendfollowing.py


<a id="org8032172"></a>

# API:

To be filled here
<give link to our website here>

