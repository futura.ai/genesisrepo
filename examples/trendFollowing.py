### Futura Trend Following Trading System Example
# import necessary Packages below:
import numpy
import futuraAiToolbox.futuraAiToolbox as ftools


def tradingStrategy(DATE, OPEN, HIGH, LOW, CLOSE, VOL, exposure, equity,
                    settings):
    ''' This system uses trend following techniques to allocate capital into the desired equities'''

    nMarkets = CLOSE.shape[1]

    periodLonger = 50
    periodShorter = 10

    # Calculate Simple Moving Average (SMA)

    smaLongerPeriod = numpy.nansum(
        CLOSE[-periodLonger:, :], axis=0) / periodLonger
    smaShorterPeriod = numpy.nansum(
        CLOSE[-periodShorter:, :], axis=0) / periodShorter

    longEquity = smaShorterPeriod > smaLongerPeriod
    shortEquity = ~longEquity

    pos = numpy.zeros(nMarkets)
    #print 'pos-1',pos, 'long',longEquity
    pos[longEquity] = 1
    #print 'pos-2',pos, 'short',shortEquity
    pos[shortEquity] = -1
    #print 'pos-3',pos, 'long',shortEquity
    weights = pos / numpy.nansum(abs(pos))
    #print pos,weights
    #exit()
    return weights, settings


def stratSettings():
    ''' Define your trading system settings here '''

    settings = {}
    settings['markets'] = ['F_US', 'AAPL']
    settings['beginInSample'] = '20120101'
    settings['endInSample'] = '20170301'
    settings['lookback'] = 30
    settings['budget'] = 10**6
    settings['slippage'] = 0.05
    settings['backend'] = 'bokeh'
    # settings['backend'] = 'tk'

    return settings


# Evaluate trading system defined in current file.
if __name__ == '__main__':
    #import futuraAiToolbox.futuraAiToolbox as ftools
    results = ftools.evalTradingStrat(__file__)
