* Futuraai python api for quant trading.  
  For more information go to our website

  www.futura.ai

* Installation guide
 
** For users:
   Simply do

   ```python
   pip install futuraAiToolbox
   ```


   
** For developers
  
  **It is highly recommended that user has a virtual environment setup**
*** Virtual environment 
    For more info on virtualenvs it's  recommended to read
    http://docs.python-guide.org/en/latest/dev/virtualenvs/
*** Install the requirements from the 'requirements.txt' file
*** Clone the latest version from gitlab:
    ```shell
    git clone https://gitlab.com/futura.ai/genesisrepo.git

    ```
*** Make changes!!

    

** Install the code without pip:
   After you clone the git repository
   On the main directory do 
   ```python
    python setup.py install
   ```


* Running the Examples:

  For running the example
  python examples\trendfollowing.py


* API:
  To be filled here
  <give link to our website here>
